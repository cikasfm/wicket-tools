/**
 * 
 */
package org.cikas.wicket.resource.loader;

import org.apache.wicket.util.tester.WicketTester;
import org.cikas.wicket.test.WicketTestApplication;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author zvilutis
 *
 */
public class WicketTestCase {
    
    protected static WicketTester tester;
    
    @BeforeClass
    public static void initClass() {
        tester = new WicketTester( new WicketTestApplication() );
    }
    
    @Test
    public void testNothing() {
        // don't test me
    }

}
