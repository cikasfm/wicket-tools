/**
 * 
 */
package org.cikas.wicket.resource.loader;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.cikas.wicket.tools.core.IStringResourceContainer;
import org.cikas.wicket.tools.dao.IStringPropertyDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @author zvilutis
 *
 */
@RunWith( PowerMockRunner.class )
public class DBStringResourceLoaderTest extends WicketTestCase {
    
    DBStringResourceLoader classUnderTest;
    
    @Mock
    IStringPropertyDao stringPropertyDao;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks( this );
        
        classUnderTest = new DBStringResourceLoader();
        classUnderTest.setStringPropertyDao( stringPropertyDao );
    }

    /**
     * Test method for {@link org.cikas.wicket.resource.loader.DBStringResourceLoader#loadStringResource(org.apache.wicket.Component, java.lang.String, java.util.Locale, java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testLoadStringResourceComponentStringLocaleStringString1() {
        
        // test case # 1
        
        Component component = new WebMarkupContainer( "id" );
        
        tester.startComponent( component );
        
        classUnderTest.loadStringResource( component, null, null, null, null );
        
        verify( stringPropertyDao ).findValue( eq( component.getClass().getName() ), anyString(), anyString(), anyString(), anyString() );
        
    }

    @Test
    public final void testLoadStringResourceComponentStringLocaleStringString2() {
        
        // test case # 2 - parent implements IStringResourceContainer

        Component component = new WebMarkupContainer( "id" );
        
        StringContainerComponent parent = new StringContainerComponent( "parent", component );
        
        tester.startComponent( parent );
        
        classUnderTest.loadStringResource( parent, null, null, null, null );
        
        verify( stringPropertyDao ).findValue( eq( parent.getClass().getName() ), anyString(), anyString(), anyString(), anyString() );
        
    }
    

    @Test
    public final void testLoadStringResourceComponentStringLocaleStringString3() {
        
        // test case # 3 - parent implements IStringResourceContainer, child still uses parent's class name
        
        Component component = new WebMarkupContainer( "id" );
        
        StringContainerComponent parent = new StringContainerComponent( "parent", component );
        
        classUnderTest.loadStringResource( component, null, null, null, null );
        
        verify( stringPropertyDao ).findValue( eq( parent.getClass().getName() ), anyString(), anyString(), anyString(), anyString() );
    }

    /**
     * Test method for {@link org.cikas.wicket.resource.loader.DBStringResourceLoader#loadStringResource(java.lang.Class, java.lang.String, java.util.Locale, java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testLoadStringResourceClassOfQStringLocaleStringString1() {
        
        Component component = new WebMarkupContainer( "id" );
        
        classUnderTest.loadStringResource( component.getClass(), null, null, null, null );
        
        verify( stringPropertyDao ).findValue( eq( component.getClass().getName() ), anyString(), anyString(), anyString(), anyString() );
        
    }

    /**
     * Test method for {@link org.cikas.wicket.resource.loader.DBStringResourceLoader#loadStringResource(java.lang.Class, java.lang.String, java.util.Locale, java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testLoadStringResourceClassOfQStringLocaleStringString2() {
        
        Component component = new WebMarkupContainer( "id" );
        
        StringContainerComponent parent = new StringContainerComponent( "parent", component );
        
        classUnderTest.loadStringResource( parent.getClass(), null, null, null, null );
        
        verify( stringPropertyDao ).findValue( eq( parent.getClass().getName() ), anyString(), anyString(), anyString(), anyString() );
    }
    
    class StringContainerComponent extends WebMarkupContainer implements IStringResourceContainer {

        private static final long serialVersionUID = 1L;
        
        public final Component child;

        public StringContainerComponent( String id, Component child ) {
            super( id );
            this.child = child;
            add( child );
        }
        
    }

}
