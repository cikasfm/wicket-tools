/**
 * 
 */
package org.cikas.wicket.test;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

/**
 * @author zvilutis
 *
 */
public class WicketTestApplication extends WebApplication {

    /* (non-Javadoc)
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends Page> getHomePage() {
        return null;
    }

}
