/**
 * 
 */
package org.cikas.wicket.renderers;

import org.apache.wicket.extensions.markup.html.form.select.IOptionRenderer;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.cikas.wicket.tools.domain.ILookupValue;

/**
 * {@link LookupValueOptionRenderer}
 * 
 * @author ZilvinasV
 *         Created on Mar 23, 2010
 * 
 */
public final class LookupValueOptionRenderer implements IOptionRenderer<ILookupValue> {

    private static final long serialVersionUID = 6365592553928526051L;

    private static LookupValueOptionRenderer instance = new LookupValueOptionRenderer();

    public static LookupValueOptionRenderer getInstance() {
        return instance;
    }

    private LookupValueOptionRenderer() {
        // private constructor
    }

    /* (non-Javadoc)
     * @see org.apache.wicket.extensions.markup.html.form.select.IOptionRenderer#getDisplayValue(java.lang.Object)
     */
    public String getDisplayValue( ILookupValue object ) {
        return object.getDisplayValue();
    }

    /* (non-Javadoc)
     * @see org.apache.wicket.extensions.markup.html.form.select.IOptionRenderer#getModel(java.lang.Object)
     */
    public IModel<ILookupValue> getModel( final ILookupValue value ) {
        return new Model<ILookupValue>( value );
    }
}