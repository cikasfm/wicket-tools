/**
 * 
 */
package org.cikas.wicket.renderers;

import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AbstractAutoCompleteRenderer;
import org.apache.wicket.request.Response;
import org.cikas.wicket.tools.domain.IDisplayable;

/**
 * {@link DisplayableAutoCompleteRenderer}
 * 
 * @author ZilvinasV
 *         Created on Mar 22, 2010
 * 
 */
public class DisplayableAutoCompleteRenderer extends AbstractAutoCompleteRenderer<IDisplayable> {

    private static final long serialVersionUID = 7444373704050795305L;

    private static DisplayableAutoCompleteRenderer instance = new DisplayableAutoCompleteRenderer();

    private DisplayableAutoCompleteRenderer() {
        // default private constructor to disable instantiation
    }

    public static DisplayableAutoCompleteRenderer getInstance() {
        return instance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.wicket.extensions.ajax.markup.html.autocomplete.AbstractAutoCompleteRenderer#getTextValue(java.lang
     * .Object)
     */
    @Override
    protected String getTextValue( IDisplayable object ) {
        return object.getDisplayValue();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.wicket.extensions.ajax.markup.html.autocomplete.AbstractAutoCompleteRenderer#renderChoice(java.lang
     * .Object, org.apache.wicket.Response, java.lang.String)
     */
    @Override
    protected void renderChoice( IDisplayable object, Response response, String criteria ) {
        String textValue = getTextValue( object );
        if ( criteria.length() > 0 ) {
            String[] words = textValue.split( " " );
            String[] criterias = criteria.split( " " );

            StringBuilder sb = new StringBuilder();
            for ( String word : words ) {
                boolean match = false;
                for ( String searchName : criterias ) {
                    if ( !match ) {
                        int splitAt = ( searchName.length() <= word.length() ) ? searchName.length() : word.length();
                        if ( word.toLowerCase().startsWith( searchName.toLowerCase() ) ) {
                            sb.append( "<em>" )
                                    .append( word.substring( 0, splitAt ) )
                                    .append( "</em>" )
                                    .append( word.substring( splitAt ) );
                            match = true;
                            break;
                        }
                    }
                }
                if ( !match ) {
                    sb.append( word );
                }
                sb.append( " " );
            }
            response.write( sb.toString() );
        } else {
            response.write( textValue );
        }
    }

}
