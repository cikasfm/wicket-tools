/**
 * 
 */
package org.cikas.wicket.util;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.wicket.Session;

/**
 * {@link DisplayValueUtils}
 * 
 * @author ZilvinasV
 *         Created on Feb 27, 2010
 * 
 */
public abstract class DisplayValueUtils {
    
    private DisplayValueUtils() {
        // will make the class non-extendable
    }

    /**
     * Resolves enumeration display value from bundle by its qualified name. E.g. if enum is org.smexy.domain.Gender
     * then it tries to find a bundle in the same package named Gender.properties ( org/project/domain/Gender.properties )
     * 
     * @param enumClassName
     * @param enumValue
     * @return
     */
    public static String getEnumDisplayValue( final String enumClassName, final String enumValue ) {
        return ResourceBundle.getBundle( enumClassName, Session.exists() ? Session.get().getLocale() : Locale.getDefault() ).getString( enumValue );
    }

}
