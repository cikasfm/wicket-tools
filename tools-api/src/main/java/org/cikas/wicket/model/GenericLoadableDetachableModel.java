/**
 * 
 */
package org.cikas.wicket.model;

import org.apache.wicket.model.LoadableDetachableModel;
import org.cikas.wicket.tools.dao.IGenericDao;
import org.cikas.wicket.tools.domain.IPersistent;

/**
 * {@link GenericLoadableDetachableModel} by an example at
 * <a>http://stronglytypedblog.blogspot.com/2009/03/wicket-patterns-and-pitfalls-1.html</a>
 * 
 * @author ZilvinasV
 *         Created on May 12, 2010
 * 
 */
@SuppressWarnings( "unchecked" )
public class GenericLoadableDetachableModel<T extends IPersistent> extends LoadableDetachableModel {

    private static final long serialVersionUID = -4486677083294716781L;

    private IGenericDao dao;

    private final Class<T> entityClass;

    private final Long entityId;

    public GenericLoadableDetachableModel( T entity ) {
        super( entity );
        
        entityClass = ( Class<T> ) entity.getClass();
        entityId = null;
    }

    public GenericLoadableDetachableModel( Class<T> entityClass, Long entityId ) {
        super();
        
        this.entityClass = entityClass;
        this.entityId = entityId;
    }

    protected T load() {
        return ( T ) dao.get( entityClass, entityId );
    }
    
    public void setDao( IGenericDao dao ) {
        this.dao = dao;
    }

}
