/**
 * 
 */
package org.cikas.wicket.tools.dao;

import java.util.List;
import java.util.Map;

import org.cikas.wicket.tools.domain.ILookupDefinition;
import org.cikas.wicket.tools.domain.ILookupValue;

/**
 * ILookupDao
 * 
 * @author ZilvinasV
 *         Created on Mar 18, 2010
 * 
 */
public interface ILookupDao {

    /**
     * Finds a {@link LookupDefinition} by lookup name with all lookup values if it exists or returns NULL otherwise
     * 
     * @param lookupName
     * @return
     */
    ILookupDefinition findLookupByName( String lookupName );
    
    /**
     * Gets all available display values for the given lookup
     * 
     * @param lookupName
     * @return
     */
    List<ILookupValue> findLookupValues( String lookupName );
    
    /**
     * Gets all available display values for the given lookup by the given parameters
     * 
     * @param lookupName
     * @param params
     * @return
     */
    List<ILookupValue> findLookupValues( String lookupName, Map<String, String> params );

    /**
     * Gets all available display values for the given lookup as a code -> value map
     * 
     * @param lookupName
     * @return
     */
    Map<String, String> findDisplayValues( String lookupName );

    /**
     * Resolves lookup value map by name and additional parameters where code is the key
     * 
     * @param lookupName
     * @param params
     * @return
     */
    Map<String, String> findDisplayValues( String lookupName, Map<String, String> params );

    /**
     * Finds a display value for a lookup by its name and code
     * 
     * @param lookupName
     * @param code
     * @return
     */
    String findDisplayValue( String lookupName, String code );

    /**
     * Finds a display value for a lookup by its name and code and the given parameters
     * 
     * @param lookupName
     * @param code
     * @param params
     * @return
     */
    String findDisplayValue( String lookupName, String code, Map<String, String> params );

}
