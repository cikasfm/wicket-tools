/**
 * 
 */
package org.cikas.wicket.tools.domain;


/**
 * {@link ILookupValue}
 *
 * @author ZilvinasV
 * Created on Mar 18, 2010
 * 
 */
public interface ILookupValue extends IPersistent, IDisplayable {
    
    String getCode();

}