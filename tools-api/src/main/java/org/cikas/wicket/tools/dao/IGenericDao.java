/**
 * 
 */
package org.cikas.wicket.tools.dao;

import java.util.List;

import org.cikas.wicket.tools.domain.IPersistent;

/**
 * {@link IGenericDao} by an example at
 * <a>http://stronglytypedblog.blogspot.com/2009/03/wicket-patterns-and-pitfalls-1.html</a>
 *
 * @author ZilvinasV
 * Created on May 12, 2010
 * 
 */
public interface IGenericDao<T extends IPersistent> {
    
    /**
     * @param entityClass
     * @param entityId
     * @return
     */
    T get( Class<T> entityClass, Long entityId );
    
    /**
     * @param entityClass
     * @param first
     * @param count
     * @return
     */
    List<T> findAll( Class<T> entityClass, int first, int count );
    
    /**
     * @param entityClass
     * @return
     */
    int count( Class<T> entityClass );
    
}
