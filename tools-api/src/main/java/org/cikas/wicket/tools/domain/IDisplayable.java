package org.cikas.wicket.tools.domain;

/**
 * An interface marking that the object can be displayable in UI.
 * 
 * @author Zilva
 *
 */
public interface IDisplayable {
    
    String getDisplayValue();

}
