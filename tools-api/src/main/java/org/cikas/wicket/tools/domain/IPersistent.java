package org.cikas.wicket.tools.domain;

import java.io.Serializable;


/**
 * super interface for all persistent entities
 * 
 * @author zvilutis
 */
public interface IPersistent extends Serializable {

    Long getId();

    void setId( Long id );
}
