/**
 * 
 */
package org.cikas.wicket.tools.dao;

import java.util.List;

import org.cikas.wicket.tools.domain.IStringProperty;

/**
 * IStringPropertyDao
 *
 * @author ZilvinasV
 * Created on Apr 19, 2010
 * 
 */
public interface IStringPropertyDao  {

    String findValue( String clazz, String key, String locale, String style, String variation );

    IStringProperty findProperty( String clazz, String key, String locale, String style, String variation );

    IStringProperty findProperty( IStringProperty example );

    List<IStringProperty> findProperties( String clazz, String key, String locale, String style, String variation );

    void save( IStringProperty stringProperty );

}
