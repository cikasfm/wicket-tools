package org.cikas.wicket.tools.domain;

public interface IStringProperty {

    String getClazz();

    void setClazz( String clazz );

    String getKey();

    void setKey( String key );

    String getLocale();

    void setLocale( String locale );

    String getStyle();

    void setStyle( String style );

    String getValue();

    void setValue( String value );

    String getDisplayValue();

}