package org.cikas.wicket.tools.domain;

import java.util.List;

/**
 * {@link ILookupDefinition}
 *
 * @author ZilvinasV
 * Created on Mar 18, 2010
 * 
 */
public interface ILookupDefinition extends IPersistent {
    
    String getLookupName();

    String getLookupClass();
    
    List<ILookupValue> getLookupValues();
    
}
