/**
 * 
 */
package org.cikas.wicket.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteSettings;
import org.apache.wicket.model.Model;
import org.cikas.wicket.renderers.DisplayableAutoCompleteRenderer;
import org.cikas.wicket.tools.dao.ILookupDao;
import org.cikas.wicket.tools.domain.ILookupDefinition;
import org.cikas.wicket.tools.domain.ILookupValue;

/**
 * {@link AutoCompleteLookupField} an extension of {@link AbstractAutoCompleteTextField} used for {@link ILookupValue}
 * listings
 * 
 * @author ZilvinasV
 *         Created on Mar 22, 2010
 * 
 */
public class AutoCompleteLookupField extends AbstractAutoCompleteTextField<ILookupValue> {

    private static final long serialVersionUID = 2712886289850518567L;

    private static final AutoCompleteSettings AUTO_COMPLETE_SETTINGS = new AutoCompleteSettings();

    static {
        AUTO_COMPLETE_SETTINGS.setCssClassName( "suggest" );
        AUTO_COMPLETE_SETTINGS.setAdjustInputWidth( true );
        AUTO_COMPLETE_SETTINGS.setMaxHeightInPx( 400 );
        AUTO_COMPLETE_SETTINGS.setPreselect( false );
        AUTO_COMPLETE_SETTINGS.setThrottleDelay( 200 );
    }

    private final String lookupName;

    private ILookupDao lookupDao;

    /**
     * @param id
     * @param lookupName
     */
    public AutoCompleteLookupField( String id, String lookupName ) {
        this( id, lookupName, AUTO_COMPLETE_SETTINGS );
    }

    /**
     * @param id
     * @param lookupName
     * @param settings
     */
    public AutoCompleteLookupField( String id, String lookupName, AutoCompleteSettings settings ) {
        super( id, new Model<ILookupValue>( null ), null, DisplayableAutoCompleteRenderer.getInstance(), settings );
        this.lookupName = lookupName;
        
        if ( lookupName == null || lookupName.trim().length() == 0 ) {
            throw new IllegalArgumentException( "Lookup Name must not be null!" );
        }
    }

    @Override
    protected List<ILookupValue> getChoiceList( String searchTextInput ) {
        final ILookupDefinition lookup = lookupDao.findLookupByName( lookupName );
        if ( lookup != null ) {
            return lookup.getLookupValues();
        } else {
            return new ArrayList<ILookupValue>( 0 );
        }
    }

    @Override
    protected String getChoiceValue( ILookupValue choice ) throws Throwable {
        return choice.getCode();
    }
    
    public void setLookupDao( ILookupDao lookupDao ) {
        this.lookupDao = lookupDao;
    }

}
