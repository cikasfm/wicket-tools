package org.cikas.wicket.components;
/**
 * 
 */


import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.model.LoadableDetachableModel;

/**
 * {@link ExternalImage}
 * 
 * @author ZilvinasV
 *         Created on Apr 5, 2010
 * 
 */
public class ExternalImage extends WebComponent {

    private static final long serialVersionUID = 1L;

    public ExternalImage( String id, final String imageUrl ) {
        super( id );
        add( new AttributeModifier( "src", true, new LoadableDetachableModel<String>() {

            private static final long serialVersionUID = 1L;

            @Override
            protected String load() {
                return imageUrl;
            }
        } ) );
        setVisible( !( imageUrl == null || imageUrl.equals( "" ) ) );
    }
    
    protected void onComponentTag( ComponentTag tag ) {
        super.onComponentTag( tag );
        checkComponentTag( tag, "img" );
    }

}
