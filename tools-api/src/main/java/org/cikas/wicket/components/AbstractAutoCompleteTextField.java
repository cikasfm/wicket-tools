package org.cikas.wicket.components;

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteBehavior;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteSettings;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.IAutoCompleteRenderer;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.StringAutoCompleteRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Auto-Complete text field that allows capture of choice selections (rather than just strings). Replacement for
 * {@link org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField}.
 * 
 * @author zvilutis
 * @param <T>
 *            the choice model object type
 */
@SuppressWarnings( "unchecked" )
public abstract class AbstractAutoCompleteTextField<T> extends TextField {

    private static final Logger LOG = LoggerFactory.getLogger( AbstractAutoCompleteTextField.class );
    private static final long serialVersionUID = 1L;
    private final AutoCompleteChoiceBehavior autoCompleteBehavior;
    private transient List<T> choiceList;

    public AbstractAutoCompleteTextField( final String id, final Class<?> type ) {
        this( id, ( IModel ) null, type, false );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model, final Class<?> type, final boolean preselect ) {
        this( id, model, type, StringAutoCompleteRenderer.INSTANCE, preselect );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model, final Class<?> type, final AutoCompleteSettings settings ) {
        this( id, model, type, StringAutoCompleteRenderer.INSTANCE, settings );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model, final boolean preselect ) {
        this( id, model, ( Class<?> ) null, preselect );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model, final AutoCompleteSettings settings ) {
        this( id, model, ( Class<?> ) null, settings );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model ) {
        this( id, model, ( Class<?> ) null, false );
    }

    public AbstractAutoCompleteTextField( final String id, final boolean preselect ) {
        this( id, ( IModel ) null, preselect );
    }

    public AbstractAutoCompleteTextField( final String id, final AutoCompleteSettings settings ) {
        this( id, ( IModel ) null, settings );
    }

    public AbstractAutoCompleteTextField( final String id ) {
        this( id, ( IModel ) null, false );
    }

    public AbstractAutoCompleteTextField( final String id, final IAutoCompleteRenderer renderer ) {
        this( id, ( IModel ) null, renderer );
    }

    public AbstractAutoCompleteTextField( final String id, final Class<?> type, final IAutoCompleteRenderer renderer ) {
        this( id, null, type, renderer, false );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model, final IAutoCompleteRenderer renderer ) {
        this( id, model, ( Class<?> ) null, renderer, false );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model, final Class<?> type, final IAutoCompleteRenderer renderer,
            final boolean preselect ) {
        this( id, model, type, renderer, new AutoCompleteSettings().setPreselect( preselect ) );
    }

    public AbstractAutoCompleteTextField( final String id, final IModel model, final Class<?> type, final IAutoCompleteRenderer renderer,
            final AutoCompleteSettings settings ) {
        super( id, model );
        setType( type );

        add( new SimpleAttributeModifier( "autocomplete", "off" ) );

        autoCompleteBehavior = createAutoCompleteBehavior( renderer, settings );
        if ( autoCompleteBehavior == null ) {
            throw new NullPointerException( "Auto complete behavior cannot be null" );
        }
        add( autoCompleteBehavior );
    }

    protected AutoCompleteChoiceBehavior createAutoCompleteBehavior( final IAutoCompleteRenderer renderer, final AutoCompleteSettings settings ) {
        return new AutoCompleteChoiceBehavior( renderer, settings );
    }

    public final AutoCompleteChoiceBehavior getAutoCompleteBehavior() {
        return autoCompleteBehavior;
    }

    /**
     * Call-back method that should return an iterator over all possible assist choice objects. These objects will be
     * passed to the renderer to generate output. Usually it is enough to return an
     * iterator over strings.
     * 
     * @see org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteBehavior#getChoices(String)
     * @param searchTextInput
     *            current input
     * @return iterator over all possible choice objects
     */
    private final Iterator<T> getChoices( final String searchTextInput ) {
        // find list of items to display in auto-complete (we need to cache the list because the auto-complete only
        // deals with strings)
        choiceList = getChoiceList( searchTextInput );
        return choiceList.iterator();
    }

    /**
     * Call-back method that should return a list of all possible assist choice objects. These objects will be passed to
     * the renderer to generate output.
     * 
     * @param searchTextInput
     *            current input text
     * @return iterator over all possible choice objects
     */
    protected abstract List<T> getChoiceList( final String searchTextInput );

    /**
     * Gets the string value from the specified choice
     * 
     * @param choice
     *            the choice that needs value extraction
     * @return the unique string value of the choice
     * @throws Throwable
     *             any error that may occur during choice extraction
     */
    protected abstract String getChoiceValue( final T choice ) throws Throwable;

    /**
     * Finds the selection by cycling through the current choices and matching the choices value. If the selected choice
     * is found the choices will be reset and the choice will be returned.
     * <p>
     * <b>NOTE:</b> Assumes that the selection choice values are unique
     * </p>
     * 
     * @return the found selection model by name (null if it cannot be found)
     */
    public final T findChoice() {
        try {
            if ( choiceList != null ) {
                for ( final T choice : choiceList ) {
                    if ( getConvertedInput().equals( getChoiceValue( choice ) ) ) {
                        return choice;
                    }
                }
                LOG.debug( "Unable to find choice selection for \"{}\"", getModelObject() );
            } else {
                LOG.debug( "Choice list is empty \"{}\"", getModelObject() );
            }
        } catch ( final Throwable e ) {
            LOG.error( "Unable to find choice selection", e );
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterRender() {
        super.onAfterRender();
        // clear choices cache
        choiceList = null;
    }

    /**
     * Auto-complete behavior that implements the choice iteration
     * 
     * @author zvilutis
     */
    public class AutoCompleteChoiceBehavior extends AutoCompleteBehavior {

        private static final long serialVersionUID = 1L;

        public AutoCompleteChoiceBehavior( final IAutoCompleteRenderer renderer, final AutoCompleteSettings settings ) {
            super( renderer, settings );
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected final Iterator<?> getChoices( final String input ) {
            return AbstractAutoCompleteTextField.this.getChoices( input );
        }
    }
}