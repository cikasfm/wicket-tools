/**
 * 
 */
package org.cikas.wicket.resource.loader;

import java.util.Locale;

import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.resource.loader.IStringResourceLoader;
import org.cikas.wicket.tools.core.IStringResourceContainer;
import org.cikas.wicket.tools.dao.IStringPropertyDao;

/**
 * DBStringResourceLoader
 *
 * @author ZilvinasV
 * Created on Apr 19, 2010
 */
public class DBStringResourceLoader implements IStringResourceLoader {
    
    private IStringPropertyDao stringPropertyDao;
    
    public DBStringResourceLoader() {
        // Injector.get().inject(this);
    }

    /* (non-Javadoc)
     * @see org.apache.wicket.resource.loader.IStringResourceLoader#loadStringResource(org.apache.wicket.Component, java.lang.String, java.util.Locale, java.lang.String, java.lang.String)
     */
    public String loadStringResource( Component component, String key, Locale locale, String style, String variation ) {

        Class<? extends Component> clazz = null;
        if ( component != null ) {

            Component parent = component;

            while ( parent != null ) {
                if ( parent instanceof IStringResourceContainer ) {
                    clazz = parent.getClass();
                    break;
                }
                if ( parent instanceof Page ) {
                    clazz = parent.getClass();
                    break;
                }
                parent = parent.getParent();
            }
            if ( clazz == null ) {
                clazz = component.getClass();
            }
        }
        return loadStringResource( clazz, key, locale, style, variation );
    }

    /* (non-Javadoc)
     * @see org.apache.wicket.resource.loader.IStringResourceLoader#loadStringResource(java.lang.Class, java.lang.String, java.util.Locale, java.lang.String, java.lang.String)
     */
    public String loadStringResource( Class<?> clazz, String key, Locale locale, String style, String variation ) {
        final String className = clazz != null ? clazz.getName() : null;
        final String localeString = locale != null ? locale.toString() : null;

        return stringPropertyDao.findValue( className, key, localeString, style, variation );
    }

    public void setStringPropertyDao( IStringPropertyDao stringPropertyDao ) {
        this.stringPropertyDao = stringPropertyDao;
    }

}
